<?php

session_start();

if (!isset($_SESSION['user'])){
    echo "<script>alert('Please login to continue');
    window.location.replace('index.php');</script>";
    die;
};

session_abort();
?>

