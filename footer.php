<?php
/**
 * Created by PhpStorm.
 * User: edmun
 * Date: 11/4/2018
 * Time: 2:25 PM
 */
?>

    <script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
    <script type="text/javascript" src="js/materialize.js"></script>
    <script type="text/javascript">
        $(document).ready(function(){
            $('.parallax').parallax();
            $(".dropdown-button").dropdown({hover: true});
        });
    </script>

</main>

<footer class="page-footer grey darken-1">
    <!--<div class="container">
        <div class="row">
            <div class="col l6 s12">
                <h5 class="white-text">Footer Content</h5>
                <p class="grey-text text-lighten-4">You can use rows and columns here to organize your footer content.</p>
            </div>
            <div class="col l4 offset-l2 s12">
                <h5 class="white-text">Links</h5>
                <ul>
                    <li><a class="grey-text text-lighten-3" href="#!">Link 1</a></li>
                    <li><a class="grey-text text-lighten-3" href="#!">Link 2</a></li>
                    <li><a class="grey-text text-lighten-3" href="#!">Link 3</a></li>
                    <li><a class="grey-text text-lighten-3" href="#!">Link 4</a></li>
                </ul>
            </div>
        </div>
    </div>-->
    <div class="footer-copyright">
        <div class="container">
            © 2018 Maersk

        </div>
    </div>
</footer>

</body>

</html>