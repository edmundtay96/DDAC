<?php
/**
 * Created by PhpStorm.
 * User: edmun
 * Date: 4/11/2018
 * Time: 2:57 PM
 */
include "logincheck.php";
include_once "header.php";

?>

<div class="container">


    <table class="responsive-table highlight">
        <thead>
          <tr>
              <th>ContainerID</th>
              <th>Ship Name</th>
              <th>Port Name</th>
              <th>Arrival Time</th>
              <th>Storage Coordinates</th>
              <th>Status</th>
              <th>Actions</th>
          </tr>
        </thead>

        <tbody>
            <?php
            $sql2= "SELECT * FROM inbound i inner JOIN ship s on s.ShipID = i.ShipID inner JOIN dock d on d.DockID = i.DockID";
            $result = $conn->query($sql2);
            if ($result->num_rows > 0) {
                // output data of each row
                while($row = $result->fetch_assoc()) {
                    echo "<tr>";
                    echo "<td>" . $row["ContainerID"] . "</td>";
                    echo "<td>" . $row["ShipName"] . "</td>";
                    echo "<td>" . $row["DockName"] . "</td>";
                    echo "<td>" . $row["ArrivalTime"] . "</td>";
                    echo "<td>" . $row["StorageLocation"] . "</td>";
                    echo "<td>" . $row["Status"] . "</td>";
                    echo '<td> <a style="color: black" href="edit-Inbound.php?id=' . $row["ContainerID"] . '"><i class="material-icons">edit</i></a></td>';
                    echo "</tr>";
                }
            };
            ?>



        </tbody>
      </table>
    <br>
    <br>
</div>

<?php
include_once "footer.php"
?>