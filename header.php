<?php
/**
 * Created by PhpStorm.
 * User: edmun
 * Date: 4/11/2018
 * Time: 2:10 AM
 */

include "config.php"
?>

<!DOCTYPE html>
<html>

<head>
    <!--Import Google Icon Font-->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <!--Import materialize.css-->
    <link type="text/css" rel="stylesheet" href="css/materialize.css" media="screen,projection"/>

    <!--Let browser know website is optimized for mobile-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
</head>

<body>
    <main>
        <!-- Dropdown Structure -->
        <ul id="dropdown1" class="dropdown-content ">
            <li><a href="logout.php">Logout</a></li>
        </ul>

        <nav class ="grey darken-1">
            <div class="nav-wrapper container">
                <a href="index.php" class="brand-logo">Maersk Line CMS</a>
                <ul id="nav-mobile" class="right hide-on-med-and-down">
                    <li><a href="Inbound.php">Inbound</a></li>
                    <li><a href="outbound.php">Outbound</a></li>
                    <!-- Admin
                    <li><a href="vessel.php">Vessel</a></li>
                    <li><a href="agents.php">Agents</a></li> -->
                    <?php
                        if (isset($_SESSION['user'])){

                            echo "<li><a class=\"dropdown-button\" href=\"#!\" data-activates=\"dropdown1\">Hi, " .  $_SESSION['user'] . "<i class=\"material-icons right\">arrow_drop_down</i></a></li>";
                        }
                        else{
                            echo '<li><a href="login.php">Login</a></li>';
                        }
                    ?>
                </ul>
            </div>
        </nav>

