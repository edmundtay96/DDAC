<?php
/**
 * Created by PhpStorm.
 * User: edmun
 * Date: 4/11/2018
 * Time: 2:53 PM
 */
include_once "header.php";

if(isset($_POST['submit']))
{
    if (!empty($_POST['name']) && !empty($_POST['address']) && !empty($_POST['contact']))
    {
        $name = $_POST['name'];
        $address = $_POST['address'];
        $contact = $_POST['contact'];
        $id = $_POST['id'];
        $status = 'Awaiting departure';
        // Checks student table first
        $stmt = $conn->prepare('UPDATE `outbound` SET `GateID`= ?, `PlateNumber`= ? ,`Status` = ? WHERE `ContainerID` = ?');


        $stmt->bind_param('ssss',$address, $contact, $status , $id);

        // execute query
        $stmt->execute();

        echo "
        <script>alert('Update successfully'); 
        window.location.replace('Outbound.php');
        </script>";

    }
    else
    {
        echo "
        <script>alert('Please fill in all empty fields.'); 
        window.location.replace('Outbound.php');
        </script>";
    }
}
else if(isset($_GET['id']))
{
    $id = $_GET['id'];
    $stmt = $conn->prepare('SELECT * FROM outbound WHERE `ContainerID` = ?');

    $stmt->bind_param('s', $id);

    // execute query
    $stmt->execute();

    // Get the result
    $result = $stmt->get_result();
    $row = $result->fetch_assoc();

    if ($result->num_rows === 1)
    {
        $name = $row['ContainerID'];
        $address = $row['GateID'];
        $contact =  $row['PlateNumber'];
    };
}
?>


<br>
<div class="container">
    <div class="row">
        <div class="col s8 offset-s2">
            <div class="card">
                <div class="card-content">
                    <div class="row">
                        <form id="loginForm" method="post" class="col s12" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>">
                            <input type="hidden" name="id" id="id" value="<?php echo $id; ?>">

                            <div class="row">
                                <div class="input-field col s12">
                                    <input value="<?php echo $name?>" id="name" name="name" type="text" class="validate" readonly="readonly">
                                    <label for="name">Customer ID</label>
                                </div>
                            </div>
                            <div class="row">
                                <div class="input-field col s12">
                                    <input value="<?php echo $address?>" id="address" name="address" type="text" class="validate">
                                    <label for="address">GateID</label>
                                </div>
                            </div>
                            <div class="row">
                                <div class="input-field col s12">
                                    <input value="<?php echo $contact?>"id="contact" name="contact" type="text" class="validate">
                                    <label for="contact">PlateNumber</label>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col s12 right-align">
                                    <!--<input id="submit" name="submit" type="submit" class="waves-effect waves-light btn" value="Login">-->
                                    <button class="waves-effect waves-light btn" type="submit" name="submit">Submit</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php
include_once "footer.php"
?>