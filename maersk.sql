-- phpMyAdmin SQL Dump
-- version 4.7.9
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Apr 11, 2018 at 05:37 PM
-- Server version: 10.1.31-MariaDB
-- PHP Version: 7.2.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `maesk`
--

-- --------------------------------------------------------

--
-- Table structure for table `account`
--

CREATE TABLE `account` (
  `AccountID` varchar(5) NOT NULL,
  `UserName` varchar(100) NOT NULL,
  `Password` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `account`
--

INSERT INTO `account` (`AccountID`, `UserName`, `Password`) VALUES
('A0001', 'David Loberman', 'pass123');

-- --------------------------------------------------------

--
-- Table structure for table `container`
--

CREATE TABLE `container` (
  `ContainerID` varchar(5) NOT NULL,
  `Owner` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `container`
--

INSERT INTO `container` (`ContainerID`, `Owner`) VALUES
('C0001', 'Samsung'),
('C0002', 'Samsung'),
('C0003', 'Apple'),
('C0004', 'Acer'),
('C0005', 'Caring');

-- --------------------------------------------------------

--
-- Table structure for table `dock`
--

CREATE TABLE `dock` (
  `DockID` varchar(5) NOT NULL,
  `DockName` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `dock`
--

INSERT INTO `dock` (`DockID`, `DockName`) VALUES
('D0001', 'Port Melaka'),
('D0002', 'Port Singapore'),
('D0003', 'Port Hong Kong'),
('D0004', 'Port Lancaster');

-- --------------------------------------------------------

--
-- Table structure for table `inbound`
--

CREATE TABLE `inbound` (
  `ContainerID` varchar(5) NOT NULL,
  `ShipID` varchar(5) NOT NULL,
  `ArrivalTime` datetime NOT NULL,
  `DockID` varchar(5) NOT NULL,
  `StorageLocation` varchar(20) NOT NULL,
  `Status` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `inbound`
--

INSERT INTO `inbound` (`ContainerID`, `ShipID`, `ArrivalTime`, `DockID`, `StorageLocation`, `Status`) VALUES
('C0001', 'S0002', '2018-04-13 00:00:00', 'D0002', 'Null', 'In transit');

-- --------------------------------------------------------

--
-- Table structure for table `outbound`
--

CREATE TABLE `outbound` (
  `ContainerID` varchar(5) NOT NULL,
  `StorageLocation` varchar(50) NOT NULL,
  `DepartureTime` datetime NOT NULL,
  `GateID` varchar(4) NOT NULL,
  `PlateNumber` varchar(10) NOT NULL,
  `Status` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `outbound`
--

INSERT INTO `outbound` (`ContainerID`, `StorageLocation`, `DepartureTime`, `GateID`, `PlateNumber`, `Status`) VALUES
('C0004', '1,4,5', '2018-04-10 00:00:00', 'G3', 'WKM 6732', 'Awaiting departure'),
('C0005', '2,4,5', '2018-04-12 00:00:00', 'G2', 'bKM 3732', 'Gate Processing'),
('C0002', '1,2,3', '2018-04-10 00:00:00', 'G2', 'TAS 2313', 'Awaiting assignment');

-- --------------------------------------------------------

--
-- Table structure for table `ship`
--

CREATE TABLE `ship` (
  `ShipID` varchar(5) NOT NULL,
  `ShipName` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ship`
--

INSERT INTO `ship` (`ShipID`, `ShipName`) VALUES
('S0001', 'Kingfisher'),
('S0002', 'Cornwall'),
('S0003', 'Marathon');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `account`
--
ALTER TABLE `account`
  ADD PRIMARY KEY (`AccountID`);

--
-- Indexes for table `container`
--
ALTER TABLE `container`
  ADD PRIMARY KEY (`ContainerID`);

--
-- Indexes for table `dock`
--
ALTER TABLE `dock`
  ADD PRIMARY KEY (`DockID`);

--
-- Indexes for table `inbound`
--
ALTER TABLE `inbound`
  ADD PRIMARY KEY (`DockID`,`StorageLocation`),
  ADD KEY `ContainerID` (`ContainerID`),
  ADD KEY `ShipID` (`ShipID`);

--
-- Indexes for table `outbound`
--
ALTER TABLE `outbound`
  ADD KEY `ContainerID` (`ContainerID`);

--
-- Indexes for table `ship`
--
ALTER TABLE `ship`
  ADD PRIMARY KEY (`ShipID`);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `inbound`
--
ALTER TABLE `inbound`
  ADD CONSTRAINT `inbound_ibfk_1` FOREIGN KEY (`ContainerID`) REFERENCES `container` (`ContainerID`),
  ADD CONSTRAINT `inbound_ibfk_2` FOREIGN KEY (`ShipID`) REFERENCES `ship` (`ShipID`),
  ADD CONSTRAINT `inbound_ibfk_3` FOREIGN KEY (`DockID`) REFERENCES `dock` (`DockID`);

--
-- Constraints for table `outbound`
--
ALTER TABLE `outbound`
  ADD CONSTRAINT `outbound_ibfk_1` FOREIGN KEY (`ContainerID`) REFERENCES `container` (`ContainerID`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
