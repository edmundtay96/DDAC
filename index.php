<?php
/**
 * Created by PhpStorm.
 * User: edmun
 * Date: 11/4/2018
 * Time: 2:20 PM
 */

    include_once "header.php"
?>

    <div class="parallax-container">
        <div class="parallax"><img class="responsive-img" src="images/MaerskLanding.jpg"></div>
        <div class="container">
            <br>
            <h1 class="header center">Maersk Line</h1>
            <div class="row center">
                <div class="col s12">The leading container company in the world</div>
            </div>
        </div>
    </div>

    <br>
    <div class="container">
         <div class="row">
             <div class="col s6 offset-s3 center">
                 <h5>Containers and Yard Management</h5>
                 View and Manage Containers and Yard related to Maersk directly on the webpage
             </div>


         </div>
    </div>

<?php
    include_once "footer.php"
?>