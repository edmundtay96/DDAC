<?php
/**
 * Created by PhpStorm.
 * User: edmun
 * Date: 4/11/2018
 * Time: 2:59 PM
 */
include "logincheck.php";
include_once "header.php";

?>

    <div class="container">

        <table class="responsive-table highlight">
            <thead>
            <tr>
                <th>ContainerID</th>
                <th>StorageLocation</th>
                <th>DepartureTime</th>
                <th>GateID</th>
                <th>PlateNumber</th>
                <th>Status</th>
                <th>Actions</th>
            </tr>
            </thead>

            <tbody>
            <?php
            $sql2= "SELECT * FROM outbound";
            $result = $conn->query($sql2);
            if ($result->num_rows > 0) {
                // output data of each row
                while($row = $result->fetch_assoc()) {
                    echo "<tr>";
                    echo "<td>" . $row["ContainerID"] . "</td>";
                    echo "<td>" . $row["StorageLocation"] . "</td>";
                    echo "<td>" . $row["DepartureTime"] . "</td>";
                    echo "<td>" . $row["GateID"] . "</td>";
                    echo "<td>" . $row["PlateNumber"] . "</td>";
                    echo "<td>" . $row["Status"] . "</td>";
                    echo '<td> <a style="color: black" href="edit-Outbound.php?id=' . $row["ContainerID"] . '"><i class="material-icons">edit</i></a></td>';
                    echo "</tr>";
                }
            };
            ?>



            </tbody>
        </table>
        <br>
        <br>
    </div>

<?php
include_once "footer.php"
?>